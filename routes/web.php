<?php
use App\Http\Controllers\FrontendController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'FrontendController@index')->name('frontend.index');

Route::get('mails/send', 'MailController@send')->name('mail.send');

Auth::routes();

Route::middleware(['auth'])->group(function(){

  Route::get('home', 'DkcController@index')->name('home');

  Route::resource('categories', 'CategoryController');

  Route::resource('posts', 'PostController');

  Route::get('trashed-posts', 'PostController@trashed')->name('posts.trashed');

  Route::get('restore-post/{id}', 'PostController@restore')->name('posts.restore');

  Route::post('/upload/file', 'DkcController@upload')->name('dkcs.upload');

  Route::resource('activities', 'ActivityController');

	Route::resource('donates', 'DonateController');

  Route::resource('items', 'ItemController');

});

Route::middleware(['auth', 'admin'])->group(function(){

  Route::resource('users', 'UserController');
  Route::post('users/make-admin/{user}', 'UserController@makeAdmin')->name('users.make-admin');

});
