@extends('layouts.main')

@section('title', 'Users')

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Users</div>
      <a href="{{route('users.create')}}" class="btn btn-secondary">Add</a>
    </h4>
    <div class="card-body">
      @if($users->count())
      <table class="table mt-2">
        <thead>
          <tr>
            <th>#</th>
            <th>Image</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users AS $user)
          <tr>
            <td>{{$user->id}}</td>
            <td><img src="{{Gravatar::src($user->email)}}" class="img-fluid rounded-circle"></td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
              {{$user->role}}
              @if(!$user->isAdmin())
                <form action="{{route('users.make-admin',$user->id)}}" method="post">
                  @csrf
                  <button type="submit" class="btn btn-primary">Make Admin</button>
                </form>
              @endif
            </td>
            <td>
              <form action="{{route('users.destroy', $user->id)}}" method="post" class="d-inline-block ml-2">
                  <div class="btn-group" role="group">
  
                    <a href="{{route('users.show', $user->id)}}" class="btn  btn-outline-primary btn-sm">View</a>
                    <a href="{{route('users.edit', $user->id)}}" class="btn  btn-outline-secondary btn-sm">Edit</a>
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                  </div>
                </form>              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
        <h2 class="text-center">No Users Yet</h2>
      @endif
    </div>
  </div>
@endsection