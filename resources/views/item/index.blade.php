@extends('layouts.main')

@section('title', 'รายการบุญ')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
	<div class="card">
		<div class="card-header d-flex">
			<div class="mr-auto">
				<h4>รายการบุญ</h4>
			</div>
			<a href="{{route('items.create')}}" class="btn btn-primary"><i class="fa fa-plus mr-1"></i> เพิ่มใหม่</a>
		</div>
		<div class="card-body">
			@if($items->count())
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>ชื่อบุญ</th>
							<th>กิจกรรม</th>
							<th>หมวด</th>
							<th>เริ่ม</th>
							<th>สิ้นสุด</th>
							<th>รูปภาพ</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($items AS $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>{{$item->activity->title}}</td>
							<td>{{$item->category->name}}</td>
							<td>{{$item->start_at}}</td>
							<td>{{$item->end_at}}</td>
							<td><img src="{{asset($item->cover_img)}}" class="" width="100"></td>
							<td>
								<form action="{{route('items.destroy', $item->id)}}" method="post" class="d-inline-block ml-2 form-delete" >
									<div class="btn-group" role="group">

										<a href="{{route('items.show', $item->id)}}" class="btn  btn-outline-primary btn-sm">ดู</a>
										<a href="{{route('items.edit', $item->id)}}" class="btn  btn-outline-secondary btn-sm">แก้ไข</a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-outline-danger btn-form-delete btn-sm">ลบ</button>
									</div>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<h4 class="text-center">ไม่มีข้อมูล</h4>
			@endif
		</div>
	</div>
@endsection
