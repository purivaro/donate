@extends('layouts.main')

@section('content')
  <div class="card">
    @if(asset($activity->cover_img))
      <img class="card-img-top" src="{{asset($activity->cover_img)}}" >
    @endif
    <div class="card-body">
      <h1>{{$activity->title}}</h1>
      <p class="mb-0"><a class="text-dark" href="{{route('frontend.show', $activity)}}">{{$activity->start_at->locale('th')->isoFormat('dd D MMM YYYY')}}</a></p>
      <p class="mb-0"><a class="text-dark" href="{{route('frontend.show', $activity)}}">{{$activity->end_at->locale('th')->isoFormat('dd D MMM YYYY')}}</a></p>
      <hr>
      <p>{{$activity->detail}}</p>
      <div class="mt-3"><a href="{{route('activities.index')}}" class="btn btn-primary">Back</a></div>
    </div>
  </div>
@endsection