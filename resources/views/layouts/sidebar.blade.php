<div class="list-group">
  
  <a href="/home" class="list-group-item list-group-item-action">Home</a>

  <a href="{{route('dkcs.index')}}" class="list-group-item list-group-item-action">เหตุการณ์</a>  


  @if(auth()->user()->isAdmin())
    <a href="{{route('users.index')}}" class="list-group-item list-group-item-action">Users</a>
  @endif

  {{-- <a href="{{route('posts.index')}}" class="list-group-item list-group-item-action">Posts</a>

  <a href="{{route('categories.index')}}" class="list-group-item list-group-item-action">Categories</a> --}}

  <a href="{{route('tags.index')}}" class="list-group-item list-group-item-action">Tags</a>


</div>
{{-- <div class="list-group mt-5">
  <a href="{{route('posts.trashed')}}" class="list-group-item list-group-item-action">Trashed Posts</a>
</div> --}}