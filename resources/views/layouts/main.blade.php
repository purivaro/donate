<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="icon" href="{{asset('assets/img/icon/dkcicon.png')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    @yield('css')
</head>
<body>
    @include('layouts.navbar')

    <div class="container">

        <div class="row my-2">
          <div class="col">
            @yield('error')
            @yield('status')
          </div>
        </div>

        <div class="row">
            {{-- <div class="col-3">
              @include('layouts.sidebar')
            </div> --}}
            <div class="col">
                @yield('content')
            </div>
        </div>
    </div>

    <script src="{{asset('js/main.js')}}"></script>
    @yield('scripts')
</body>
</html>
