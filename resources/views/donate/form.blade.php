@extends('layouts.main')

@section('title', 'รับบริจาค')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
	<div class="card">
		<div class="card-header d-flex">
			<div class="mr-auto">
				<h4>แบบฟอร์มรับบริจาค</h4>
			</div>
		</div>
		<div class="card-body">
			<form action="">
				@csrf
				<div class="form-group">
					<label for="">วันที่รับ</label>
					<input type="text" name="donate_at" id="donate_at" class="form-control" value="">
				</div>
				<div class="form-group">
					<label for="">กิจกรรม</label>
					<select name="activity_id" id="activity_id" class="form-control">
						<option value="">เลือก</option>
						@foreach ($activities as $activity)
							<option value="{{$activity->id}}">{{$activity->title}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="">บุญ</label>
					<select name="item_id" id="item_id" class="form-control">
						<option value="">เลือก</option>
						@foreach ($items as $item)
							<option value="{{$item->id}}">{{$item->title}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="">จำนวนเงิน</label>
					<input type="text" name="amount" id="amount" class="form-control" value="">
				</div>
				<div class="form-group">
					<label for="">สกุลเงิน</label>
					<select name="currency_id" id="currency_id" class="form-control">
						<option value="">เลือก</option>
						@foreach ($items as $item)
							<option value="{{$item->id}}">{{$item->title}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="">ประเภทการจ่าย</label>
					<select name="payment_id" id="payment_id" class="form-control">
						<option value="">เลือก</option>
						@foreach ($items as $item)
							<option value="{{$item->id}}">{{$item->title}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="">หมายเหตุ</label>
					<input type="text" name="note" id="note" class="form-control" value="">
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
@endsection

@section('scripts')
<script>

	flatpickr("#donate_at", {
		enableTime: false
	});


</script>
@endsection
