@extends('layouts.main')

@section('title', 'รับบริจาค')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
    <div class="card">
        <div class="card-header d-flex">
            <div class="mr-auto">
                <h4>รับบริจาค</h4>
            </div>
            <a href="{{route('donates.create')}}" class="btn btn-primary"><i class="fa fa-plus mr-1"></i> เพิ่มใหม่</a>
        </div>
        <div class="card-body"></div>
    </div>
@endsection
