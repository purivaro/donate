@extends('layouts.frontend.main')

@section('title', 'History072')

@section('header')
  <!-- Header -->
  {{-- <header class="header text-center text-white" style="background-image: linear-gradient(-225deg, #5D9FFF 0%, #B8DCFF 48%, #6BBBFF 100%);"> --}}
  {{-- <header class="header text-center text-white" style="background-image: linear-gradient(-225deg, #fd2630 -9%, #f9d349 50%, #ff0081 106%);"> --}}
  <header class="header text-center text-white" style="background: #ff9942e8;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <h1 class="display-2 font-k2d" style="font-weight: 500;">Dhammakaya History</h1>
          <p class="lead-3 opacity-100 mt-5 font-jamjuree">บันทึกประวัติศาสตร์วัดพระธรรมกาย</p>
        </div>
      </div>
    </div>
  </header><!-- /.header -->
  
@endsection

@section('main-content')
  <!-- Main Content -->
  <main class="main-content">

    <section class="section bg-gray">
      <div class="container">


        <div class="row">
          <div class="col-lg-8 col-xl-9">
            <div class="row">
              <div class="col">


                @forelse($dkcs AS $dkc)
                  <div class="card hover-shadow-7 mb-7">
                    <img class="card-img-top d-block d-md-none" src="{{asset($dkc->images[0]->path)}}" >
                    <div class="row">
                      <div class="col-md-4">
                        <a href="{{route('frontend.show', $dkc)}}">
                          {{-- @foreach($dkc->images AS $image) --}}
                          <img class="fit-cover position-absolute h-100" src="{{asset($dkc->images[0]->path)}}" >
                          {{-- @endforeach --}}
                        </a>
                      </div>
    
                      <div class="col-md-8">
                        <div class="p-6 px-7">
                          
                            <h4>{{$dkc->title}}</h4>
                            <p class="mb-1">
                              {{$dkc->happened_on->locale('th')->isoFormat('dd D MMM')}} {{($dkc->happened_on->isoFormat('YYYY')+543)}}
                            </p>
                            <p>
                              {{Str::words(str_replace('&nbsp;',' ', strip_tags($dkc->detail)), '8')}}
                            </p>
                          <a class="small ls-1 stretched-link" href="{{route('frontend.show', $dkc)}}">อ่านต่อ <span class="pl-1">&xrarr;</span></a>
                          <div class="mt-4">
                            @foreach($dkc->tags AS $tag)
                              <a class="badge badge-pill badge-secondary" href="{{route('frontend.tag',$tag)}}">{{$tag->name}}</a>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                
                @endforeach

              </div>
            </div>


            {{-- <div class="row gap-y">

              @forelse($dkcs AS $dkc)
                <div class="col-md-6 col-lg-4">
                  <div class="card d-block border hover-shadow-6 mb-6">
                  <a href="{{route('frontend.show', $dkc)}}">
                    @foreach($dkc->images AS $image)
                      <img class="card-img-top" src="{{asset($image->path)}}" >
                    @endforeach
                  </a>
                    <div class="p-6 text-center">
                      <h5 class="mb-0"><a class="text-dark" href="{{route('frontend.show', $dkc)}}">{{$dkc->title}}</a></h5>
                      <p class="mb-0"><a class="text-dark" href="{{route('frontend.show', $dkc)}}">{{$dkc->happened_on->locale('th')->isoFormat('dd D MMM YYYY')}}</a></p>
                      @foreach($dkc->tags AS $tag)
                        <a class="badge badge-pill badge-secondary" href="{{route('frontend.tag',$tag)}}">{{$tag->name}}</a>
                      @endforeach
                    </div>
                  </div>
                </div>
              @empty
                <div class="col-md-6 col-lg-4">
                  <p>No Result found for query <strong>{{request()->query('search')}}</strong></p>
                </div>
              @endforelse

            </div> --}}

            {{-- <nav class="flexbox mt-30">
                <a class="btn btn-white disabled"><i class="ti-arrow-left fs-9 mr-4"></i> Newer</a>
                <a class="btn btn-white" href="#">Older <i class="ti-arrow-right fs-9 ml-4"></i></a>
            </nav> --}}
            {{$dkcs->appends(['search'=>request()->query('search')])->links()}}

          </div>
          <div class="col-lg-4 col-xl-3 order-first order-lg-last">
            @include('blog.frontend.partials.sidebar')
          </div>

        </div>
        


      </div>
    </section>

  </main>
  
@endsection