@extends('layouts.frontend.main')

@section('title', $dkc->title)

@section('header')

  <!-- Header -->
  <header class="header text-white h-fullscreen pb-80" style="background-image: url({{asset($dkc->images[0]->path)}});" data-overlay="9">
    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center">

          {{-- <p class="opacity-70 text-uppercase small ls-1">{{$dkc->category->name}}</p> --}}
          <h1 class="display-4 mt-7 mb-8">{{$dkc->title}}</h1>
          <p><span class="opacity-70 mr-1">By</span> <a class="text-white" href="{{route('frontend.user',$dkc->user)}}">{{$dkc->user->name}}</a></p>
          <p><img class="avatar avatar-sm" src="{{Gravatar::src($dkc->user->email)}}" alt="..."></p>

        </div>

        <div class="col-12 align-self-end text-center">
          <a class="scroll-down-1 scroll-down-white" href="#section-content"><span></span></a>
        </div>

      </div>

    </div>
  </header><!-- /.header -->
@endsection

@section('main-content')
    <!-- Main Content -->
    <main class="main-content">

      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Blog content
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <div class="section">
          <div class="container">
  
            <div class="text-center mt-8">
              <h2>{{$dkc->title}}</h2>
              <p>{{$dkc->happened_on->locale('th')->isoFormat('dd D MMM YYYY')}}</p>
            </div>
  
  
            <div class="text-center my-8">
              @foreach($dkc->images AS $image)
                <img class="rounded-md mb-5 w-100" src="{{asset($image->path)}}" >
              @endforeach
            </div>
  
            <div class="row">
              <div class="col-lg-8 mx-auto">
  
                {!!$dkc->detail!!}
  
                {{-- Tags --}}
                <div class="gap-xy-2 mt-6">
                  @foreach($dkc->tags AS $tag)
                    <a class="badge badge-pill badge-secondary" href="{{route('frontend.tag',$tag)}}">{{$tag->name}}</a>
                  @endforeach
                </div>
  
              </div>
            </div>
            
  
  
          </div>
        </div>




      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Comments
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <div class="section bg-gray">
        <div class="container">

          <div class="row">
            <div class="col-lg-8 mx-auto">
                <div id="disqus_thread"></div>
                <script>
                
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                
                var disqus_config = function () {
                this.page.url = "{{config('app.url')}}/blogs/{{$dkc->id}}";  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = "{{$dkc->id}}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };

                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://laraveltest-4.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                            

            </div>
          </div>

        </div>
      </div>



    </main>
@endsection
