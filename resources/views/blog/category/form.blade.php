@extends('layouts.main')

@section('error')
  @if($errors->any())
    @foreach($errors->all() AS $error)  
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <form action="{{isset($category)?route('categories.update', $category->id):route('categories.store')}}" method="post">
        @csrf
        @if(isset($category))
          @method('PUT')
        @endif
        <div class="form-group">
          <label for="name">{{isset($category)?'Edit':'Create'}} Category</label>
          <input type="text" name="name" id="name" class="form-control" value="{{isset($category)?$category->name:''}}">
        </div>
        <button class="btn btn-primary">Save</button>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    document.getElementById('name').focus()
  </script>
@endsection