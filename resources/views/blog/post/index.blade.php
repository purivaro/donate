@extends('layouts.main')

@section('title', 'Posts')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Posts</div>
      <a href="{{route('posts.create')}}" class="btn btn-secondary">Add</a>
    </h4>
    <div class="card-body">
      @if($posts->count())
      <table class="table mt-2">
        <thead>
          <tr>
            <th>#</th>
            <th>Image</th>
            <th>Title</th>
            <th>Desctipion</th>
            <th>Category</th>
            <th>Tag</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($posts AS $post)
          <tr>
            <td>{{$post->id}}</td>
            <td><img src="{{asset($post->image)}}" width="100"></td>
            <td>{{$post->title}}</td>
            <td>{{$post->description}}</td>
            <td>{{$post->category->name}}</td>
            <td>
              @foreach($post->tags AS $tag)
                <a href="" class="badge badge-info">{{$tag->name}}</a>
              @endforeach
            </td>
            <td>
              <form action="{{route('posts.destroy', $post->id)}}" method="post" class="d-inline-block ml-2">
                <div class="btn-group" role="group">

                  <a href="{{route('posts.show', $post->id)}}" class="btn  btn-outline-primary btn-sm">View</a>
                  <a href="{{route('posts.edit', $post->id)}}" class="btn  btn-outline-secondary btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                </div>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {{$posts->links()}}
      @else
        <h2 class="text-center">No Posts Yet</h2>
      @endif
    </div>
  </div>
@endsection