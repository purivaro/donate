@extends('layouts.main')

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Trashed Post</div>
    </h4>
    <div class="card-body">
      
      <table class="table mt-2">
        <thead>
          <tr>
            <th>#</th>
            <th>Image</th>
            <th>Title</th>
            <th>Desctipion</th>
            <th>Content</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($trashedPosts AS $post)
          <tr>
            <td>{{$post->id}}</td>
            <td><img src="{{asset('storage/'.$post->image)}}" width="100"></td>
            <td>{{$post->title}}</td>
            <td>{{$post->description}}</td>
            <td>{!!$post->content!!}</td>
            <td>
              <a href="{{route('posts.restore', $post->id)}}" class="btn  btn-outline-info">Restore</a>
              <form action="{{route('posts.destroy', $post->id)}}" method="post" class="d-inline-block ml-2">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger">Permanently Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection