@extends('layouts.main')

@section('title', 'รวมเหตุการณ์')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">
        <h4>Dhammakaya History</h4>
        <small>บันทึกเหตุการณ์วัดพระธรรมกาย</small>
      </div>
      <a href="{{route('dkcs.create')}}" class="btn btn-primary d-flex align-items-center"><i class="fa fa-plus mr-1"></i> เพิ่มใหม่</a>
    </h4>
    <div class="card-body">
      <form action="" method="GET">
        <div class="form-group row">
          <div class="col-sm-3 mb-2">
            <input type="text" class="form-control" name="search" placeholder="ค้นหา.." value="{{request()->query('search')}}">
          </div>
          <div class="col-sm-6 d-flex mb-2">
            <select name="d" id="d" class="form-control">
              <option value="">วัน</option>
              @for($i = 1; $i <= 31; $i++)
                <option value="{{$i}}"
                  @if($i==request()->query('d'))
                    selected
                  @endif
                >{{$i}}</option>
              @endfor
            </select>
            <select name="m" id="m" class="form-control">
              <option value="">เดือน</option>
              @foreach($months as $key=>$month)
                <option value="{{$key}}"
                  @if($key==request()->query('m'))
                    selected
                  @endif
                >{{$month}}</option>
              @endforeach
            </select>
            <select name="y" id="y" class="form-control">
              <option value="">ปี</option>
              @for($i = (Carbon::now()->format('Y')+543); $i >= 2512; $i--)
                <option value="{{$i}}"
                  @if($i==request()->query('y'))
                    selected
                  @endif
                >{{$i}}</option>
              @endfor
            </select>
          </div>
          <div class="col-sm-3">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> ค้นหา</button>
          </div>
        </div>
      </form>
      @if($dkcs->count())
      <div class="table-responsive">
        <table class="table mt-4">
          <thead>
            <tr>
              <th>วันที่</th>
              <th>เหตุการณ์</th>
              <th>รายละเอียด</th>
              <th>Tag</th>
              <th>รูปภาพ</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($dkcs AS $dkc)
            <tr>
              <td class='text-nowrap'>{{$dkc->happened_on->locale('th')->isoFormat('dd D MMM')}} {{($dkc->happened_on->isoFormat('YYYY')+543)}}</td>
              {{-- <td><i>{{$dkc->happened_on->locale('th')->diffForHumans()}}</i></td> --}}
              <td>{{$dkc->title}}</td>
              <td>{{Str::words(str_replace('&nbsp;',' ', strip_tags($dkc->detail)), '8')}}
                @if(isset($dkc->vdo))
                <div><a href="{{$dkc->vdo}}" class="" target='_blank'>{{$dkc->vdo}}</a></div>
                @endif
              </td>
              <td>
                @foreach($dkc->tags AS $tag)
                  <a href="" class="badge badge-light">{{$tag->name}}</a>
                @endforeach
              </td>
              <td>
                @foreach($dkc->images AS $image)
                  <img src="{{asset($image->path)}}" width="100">
                @endforeach
              </td>
              <td>
                <form action="{{route('dkcs.destroy', $dkc->id)}}" method="post" class="d-inline-block ml-2 form-delete" >
                  <div class="btn-group" role="group">

                    <a href="{{route('dkcs.show', $dkc->id)}}" class="btn  btn-outline-primary btn-sm">ดู</a>
                    <a href="{{route('dkcs.edit', $dkc->id)}}" class="btn  btn-outline-secondary btn-sm">แก้ไข</a>
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-outline-danger btn-form-delete btn-sm">ลบ</button>
                  </div>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{$dkcs->links()}}
      @else
        <h3 class="text-center mt-4">ไม่มีข้อมูล</h3>
      @endif
    </div>
  </div>

@endsection


@section('scripts')

<script>
  $(function(){

    // confirm before delete
    $(document).on('click', '.btn-form-delete', function(e){
      e.preventDefault();
      this_ = $(this);
      return bootbox.confirm({
        message: "ยืนยันการลบข้อมูล?",
        buttons: {
            confirm: {
                label: 'ยืนยัน',
                className: 'btn-primary'
            },
            cancel: {
                label: 'ยกเลิก',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result===true){
            this_.closest('form').submit();
          }
        }
      });
    });


  });
</script>
@endsection