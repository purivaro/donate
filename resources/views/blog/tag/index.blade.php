@extends('layouts.main')

@section('title', 'Tags')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Tags</div>
      <a href="{{route('tags.create')}}" class="btn btn-secondary">Add</a>
    </h4>
    <div class="card-body">
      
      <table class="table mt-2">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Count</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categories AS $tag)
          <tr>
            <td>{{$tag->id}}</td>
            <td>{{$tag->name}}</td>
            <td>{{$tag->dkcs->count()}}</td>
            <td>
              <a href="{{route('tags.edit', $tag->id)}}" class="btn  btn-outline-info">Edit</a>
              <form action="{{route('tags.destroy', $tag->id)}}" method="post" class="d-inline-block ml-2">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection