@extends('layouts.main')

@section('title', 'Home')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Home</div>
    </h4>
    <div class="card-body">
      <h2 class="text-center">Welcome Home</h2>
    </div>
  </div>
@endsection