@if ($paginator->hasPages())
    <nav class="flexbox mt-30">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="btn btn-white disabled" aria-disabled="true">
              @lang('pagination.previous')
            </a>
        @else
            <a class="btn btn-white" href="{{ $paginator->previousPageUrl() }}" rel="prev">
              @lang('pagination.previous')
            </a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="btn btn-white" href="{{ $paginator->nextPageUrl() }}" rel="next">
              @lang('pagination.next')
            </a>
        @else
            <a class="btn btn-white disabled" aria-disabled="true">
              @lang('pagination.next')
            </a>
        @endif
  </nav>
@endif
