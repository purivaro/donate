<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
	protected $fillable = ['title', 'description', 'category_id', 'activity_id', 'start_at', 'end_at', 'user_id', 'cover_img'];

	protected $dates = [
		'start_at',
		'end_at',
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function getCoverImgAttribute($value)
	{
		return 'storage/'.$value;
	}
}
