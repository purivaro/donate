<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	protected $fillable = ['title', 'description',  'activity_id', 'category_id', 'start_at', 'end_at', 'user_id', 'cover_img'];

	protected $dates = [
		'start_at',
		'end_at',
	];

	public function activity()
	{
		return $this->belongsTo(Activity::class);
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function getCoverImgAttribute($value)
	{
		return 'storage/'.$value;
	}

}
