<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  protected $fillable = ['name'];

  public function posts()
  {
    return $this->belongsToMany(Post::class);
  }

  public function dkcs()
  {
    return $this->belongsToMany(Dkc::class);
  }
}
