<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use App\Activity;
use App\Http\Requests\Items\FormItemRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('item.index')
        ->withItems(Item::paginate(5));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			return view('item.form')
			->withActivities(Activity::all())
			->withCategories(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormItemRequest $request)
    {
			$attribute = $request->all();
			$attribute['user_id'] = auth()->user()->id;
			if($request->hasFile('cover_img')){
				$attribute['cover_img'] = $request->cover_img->store('public/items');
			}
			$item = Item::create($attribute);
			return redirect(route('items.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
			return view('item.form')
			->withItem($item)
			->withActivities(Activity::all())
			->withCategories(Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormItemRequest $request, Item $item)
    {
      $attribute = $request->all();
			$attribute['user_id'] = auth()->user()->id;
			if($request->hasFile('cover_img')){
				if($item->getOriginal('cover_img')){
					Storage::delete($item->getOriginal('cover_img'));
				}
				$attribute['cover_img'] = $request->cover_img->store('public/items');
			}
			$item->update($attribute);

			// $image = Image::make(public_path($activity->cover_img))->fit(500, 500, null, 'top');
			// $image->save();

			return redirect(route('items.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
