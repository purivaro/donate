<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\Activities\FormActivityRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('activity.index')
        ->withActivities(Activity::paginate(5));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activity.form')->withCategories(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormActivityRequest $request)
    {
			$attribute = $request->all();
			$attribute['category_id'] = 1;
			$attribute['user_id'] = auth()->user()->id;
			if($request->hasFile('cover_img')){
				$attribute['cover_img'] = $request->cover_img->store('public/activities');
			}
			$activity = Activity::create($attribute);
			return redirect(route('activities.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
      return view('activity.view')->withActivity($activity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
			return view('activity.form')
				->withActivity($activity)
				->withCategories(Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(FormActivityRequest $request, Activity $activity)
    {
      $attribute = $request->all();
			$attribute['category_id'] = 1;
			$attribute['user_id'] = auth()->user()->id;
			if($request->hasFile('cover_img')){
				if($activity->getOriginal('cover_img')){
					Storage::delete($activity->getOriginal('cover_img'));
				}
				$attribute['cover_img'] = $request->cover_img->store('public/activities');
			}
			$activity->update($attribute);

			// $image = Image::make(public_path($activity->cover_img))->fit(500, 500, null, 'top');
			// $image->save();

			return redirect(route('activities.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
			Storage::delete($activity->cover_img);
			$activity->delete();
			return redirect(route('activities.index'));
    }
}
