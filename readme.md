## Setup

composer install

php artisan key:generate

php artisan storage:link

(
	don't forget to change 
 		'default' => env('FILESYSTEM_DRIVER', 'local'), 
  to
   	'default' => env('FILESYSTEM_DRIVER', 'public'),
	in config/filesystems.php
 )

php artisan migrate

npm install

npm run dev

php artisan config:cache

php artisan serve

## Setup DB

// Log into the MySQL root administrative account.

mysql -u root -p

// Log into (on MAMP)

/Applications/MAMP/Library/bin/mysql -u root -p


// CREATE DATABASE 

CREATE DATABASE laravel DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

// CREATE USER AND PRIVILEGE

GRANT ALL ON laravel.* TO 'laraveluser'@'localhost' IDENTIFIED BY 'password';


// Flush the privileges to notify the MySQL server of the changes.

FLUSH PRIVILEGES;

// And exit MySQL.

EXIT;

